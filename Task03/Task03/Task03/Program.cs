﻿using System;

namespace week02_exercise03
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			Console.WriteLine("Please type in \"c\" for Celcius or \"f\" for fahrenheit");

			var metric = Console.ReadLine();
			const double f2c = .5556;
			const double c2f = 1.8;

			switch (metric)
			{
				case "c":
					Console.WriteLine("Type in a number to convert Celcius to Fahrenheit");
					var getc = double.Parse(Console.ReadLine());
					var roundc = System.Math.Round(getc * c2f + 32, 2);
					Console.WriteLine($"{getc} Celcius is {roundc} Fahrenheit");
					break;
				case "f":
					Console.WriteLine("Type in a number to convert Fahrenheit to Celcius");
					var getf = double.Parse(Console.ReadLine());
					var roundf = System.Math.Round(getf - 32 * f2c  , 2);
					Console.WriteLine($"{getf} Fahrenheit is {roundf} Celcius");
					break;
				default:
					Console.WriteLine("You had to type in \"c\" for Celcius or \"f\" for Fahrenheit - please run the program again");
					break;
			}
		}
	}
}
