﻿using System;

namespace Task12
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			Console.WriteLine("Enter a Number:");
			var num = Console.ReadLine();
			var a = 0; 
			bool value = int.TryParse(num, out a); 
			Console.WriteLine($"You typed in: {num}"); 
			Console.WriteLine($"Did you enter in a number? : {value}");

		}
	}
}
