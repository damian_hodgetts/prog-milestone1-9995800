﻿using System;

namespace Task10
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			var num = new string[7] { "34", "45", "21", "44", "67", "88", "86" };

			Array.Sort(num);

			Console.WriteLine(string.Join(", ", num));
		}
	}
}
