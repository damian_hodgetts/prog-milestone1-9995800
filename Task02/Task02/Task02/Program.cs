﻿using System;

namespace Task02
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			var mymonth = "";
			var myday = 0;

			Console.WriteLine("What Month were you born?");
			mymonth = Console.ReadLine();

			Console.WriteLine("What Date of that month were you born?");
			myday = int.Parse(Console.ReadLine());

			Console.WriteLine($"Hello you were born on the {myday}th of {mymonth}");



		}
	}
}
