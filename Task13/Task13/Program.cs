﻿using System;

namespace Task13
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			var colours = new string[5] { "red", "blue", "yellow", "green", "pink" };

			Array.Sort(colours);

			Console.WriteLine(string.Join(", ", colours));
		}
	}
}
